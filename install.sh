#!/usr/bin/env bash

##################################################################################################################
# Author: robbpaulsen
# Project: deb-start
# Repository: https://gitlab.com/rpbbpaulsen/reb-start
#
# Tool para automatizar la postinstalacion en kali-linux
##################################################################################################################

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors.sh

# ---------------------------------------------- Funciones ----------------------------------------------- #

ctrl_c() {
  exit
}
trap ctrl_c INT

clear
echo ""
echo -e "\t\n${BBLU}[+]${NC}${BGREEN} No ejecutar este script usando SUDO , en el transcurso de su ejecucion pedira el password para sudo pero no hacerlo antes\nPresiona CTRL + C,  de lo contrario tendras 5 segundos para detenerlo ...${NC}\n"
echo ""
sleep 5

instAller() {
  chmod u+x ~+/assets/install-1.sh
  chmod u+x ~+/assets/install-2.sh
  chmod u+x ~+/assets/install-3.sh
  chmod u+x ~+/install2.sh
  chmod u+x ~+/install3.sh
  bash ~+/assets/install-1.sh
}
instAller
