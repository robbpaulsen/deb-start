#!/usr/local/env bash

search=$(echo $@ | sed "s/ /+/g")

/usr/bin/xdg-open "https://google.com/search?q=$search" >/dev/null 2>&1 &
