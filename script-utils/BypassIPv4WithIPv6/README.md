# ipv4Bypass

*Using IPv6 to Bypass Security*

## Author `milo2012`
I came across this project on my almost very desperate attempts to find any IPv6 Project to learn 
more scripting on bash and Python making it interesting for at least a host discovery tool or port
knocker. And there's almost none known projects on the IPv6 realm, there just general known public
topics but it just covers fundamentals to the point that when finished you end up knowing exactly
the same as when you started reading the paper, writeup or watching a YT Video, to the point 
that not even UDEMY, ITPro-TV or other platforms have more information about it.

So after almost a month of getting derailed by google's new AI oriented search results, I found
6 more extended explanations and projects and how they were approached, I'll leave the links below.

By far Marc 'Van Hauser', Fernando Gont, "Tall Paul"/"Tall Paul Tech", "OneMarcFifty",
TheTechRomancer from Black Lantern Security, CVMILLER and of Course "TheEvil0ne" or "El Maligno"
spanish researcher and hacker Chema Alonso expand a lot more on the IPv6 subject as in recon,
enumeration and tooling.


https://www.youtube.com/@MalignoAlonso
EvilFoca Project

https://www.youtube.com/watch?v=6ibqE2Il1R8
TrevorProxy, BBOT and TrevorSpray

https://github.com/milo2012/ipv4Bypass
IPv6 Encapsulation Tricks

https://github.com/cvmiller
This developer besides 'Van Hauser' at least
95% of his projects are IPv6 Related

https://github.com/hackerschoice/thc-tips-tricks-hacks-cheat-sheet

https://github.com/vanhauser-thc/thc-ipv6
This project was probably the founding branch for all the 
IPv6 tooling, troubleshooting, translation and "TRICKS",
without Marc's "Van Hauser" research all would be very far
behind. Check his personal repos/projects and of course the
"THC The Hacker Choice" Repository and Projects

# https://github.com/hackerschoice
