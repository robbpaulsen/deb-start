#!/usr/bin/env bash

# ----- Script to download and parse the latest WireShark OUI Database ----- #

## ----- Functions ----- ##

### Download db
get_manuf() {
  site="https://www.wireshark.org/download/automated/data/manuf"
  wget -c ${site}
}

### Process manuf Data
data_proc() {
  local input=~+/manuf

  /usr/bin/awk '{ print $1 $2 }' ${input} | tr -d ":" | sed -r 's;(^[ #]+.*);;' >~+/wireshark_oui &&
    gzip ~+/wireshark_oui &&
    rm -f ~+/${input}
}

get_manuf &&
  data_proc
