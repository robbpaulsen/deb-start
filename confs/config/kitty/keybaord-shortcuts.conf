# vim:fileencoding=utf-8:foldmethod=marker

#: Keyboard shortcuts {{{

#: map main modifier to Control + Shift
kitty_mod ctrl+shift

#: Copy Paste functions
map kitty_mod+c copy_to_clipboard
map kitty_mod+v paste_from_clipboard

#: Paste from selection

#: map kitty_mod+s  paste_from_selection

map kitty_mod+up    scroll_line_up
map kitty_mod+page_up scroll_page_up
map kitty_mod+page_down scroll_page_down
map kitty_mod+home scroll_home
map kitty_mod+end scroll_end

#: Scroll to previous shell prompt
map kitty_mod+z scroll_to_prompt -1

#: Scroll to next shell prompt

map kitty_mod+x scroll_to_prompt 1

#: Browse output of the last shell command in pager

map kitty_mod+g show_last_command_output

#: Browse scrollback buffer in pager

map kitty_mod+h show_scrollback

#: }}}

#: Window management {{{

map kitty_mod+enter new_window
map ctrl+alt+enter launch --cwd=current
map kitty_mod+w close_window
map kitty_mod+] next_window
map kitty_mod+] previous_window

#: Move window forward
map kitty_mod+f move_window_forward

#: Move window backward
map kitty_mod+b move_window_backward

map kitty_mod+right next_tab
map kitty_mod+left previous_tab
map kitty_mod+t new_tab
map kitty_mod+q close_tab

#: Set tab title
map kitty_mod+alt+t set_tab_title

map kitty_mod+l next_layout
map ctrl+alt+t goto_layout tall
map ctrl+alt+s goto_layout stack

#: Increase font size
map kitty_mod+plus   change_font_size all +2.0
map kitty_mod+kp_add change_font_size all +2.0
map kitty_mod+minus       change_font_size all -2.0
map kitty_mod+kp_subtract change_font_size all -2.0
map kitty_mod+e open_url_with_hints


#: Open documentation site
map kitty_mod+f1 show_kitty_doc overview

#: toggle maximize screen
map kitty_mod+f10 toggle_maximized

#: toggle toggle_fullscreen
map kitty_mod+f11 toggle_fullscreen

#: Increase background opacity
map kitty_mod+a>m set_background_opacity +0.1

#: Decrease background opacity
map kitty_mod+a>l set_background_opacity -0.1

#: Reset the terminal
map kitty_mod+delete clear_terminal reset active

#: Reload kitty.conf
map kitty_mod+f5 load_config_file

#: Open kitty Website
map shift+cmd+/ open_url https://sw.kovidgoyal.net/kitty/

map kitty_mod+f10 toggle_maximized
map kitty_mod+f11 toggle_fullscreen
map kitty_mod+a>m set_background_opacity +0.1
map kitty_mod+a>l set_background_opacity -0.1

# Tab Related
map ctrl+alt+1 goto_tab 1
map ctrl+alt+2 goto_tab 2
map ctrl+alt+3 goto_tab 3
map ctrl+alt+4 goto_tab 4
map ctrl+alt+5 goto_tab 5
map ctrl+alt+6 goto_tab 6
map ctrl+alt+7 goto_tab 7
map ctrl+alt+8 goto_tab 8
map ctrl+alt+9 goto_tab 9
map ctrl+alt+o goto_tab -1

# Windows Related
map ctrl+shift+1 first_window
map ctrl+shift+2 second_window
map ctrl+shift+3 third_window
map ctrl+shift+4 fourth_window
map ctrl+shift+5 fifth_window
map ctrl+shift+6 sixth_window
map ctrl+shift+7 seventh_window
map ctrl+shift+8 eighth_window
map ctrl+shift+9 ninth_window
map ctrl+shift+0 tenth_window

#: }}}

#: }}}
