#!/usr/bin/env bash

##################################################################################################################
# Author: robbpaulsen
# Project: deb-start
# Repository: https://gitlab.com/rpbbpaulsen/reb-start
#
# Tool para automatizar la postinstalacion en kali-linux
##################################################################################################################

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors

# ---------------------------------------------- Funciones ----------------------------------------------- #

ctrl_c() {
  exit
}
trap ctrl_c INT

clear
echo ""
echo -e "\t\n${Yellow}[+]${NC}${Cyan} No ejecutar este script usando "SUDO" , en el transcurso de su ejecucion pedira el password para sudo pero no hacerlo antes\t\nPresiona CTRL + C de lo contrario, tendras 5 segundos para detenerlo de haberlo ejecutado con "SUDO" ...${NC}\n"
echo ""
sleep 5

instAller() {
  bash ~+/assets/install-3.sh
}
instAller
