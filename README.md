# deb-start

Coleccion de scripts para rapidamente configurar una instalacion recien hecha de una derivada de Debian, cubre todas las areas basicas:

- Agregar al usuario a los grupos pertinentes
- Actualizar el sistema y hacer el primer upgrade
- Instalar librerias y paquetes para emepzar a desarrollar o montar un entorno para pentesting
- Instalal Rust y Golang

En mi caso esta dirigido a un sistema kali linux, lo unico que puede diferenciar a otra instalcion de una derivada de `Debian` son los grupos,
al menos en mi experiencia si se intenta agregar al usuario a un grupo que aun no existe arroja un error y aun que se envie el stderror al 
`/dev/null` no permite proseguir con la instalacion asi que se deberia de modificar esa parte del script, haz sido advertido.

Como preferencia personal yo uso neovim y es la unica aplicacion que tiene su script por separado ya que si involucra moverse mas entre
el arbol de directorios en el sistema.
