export uHome=$(basename /home/*)

export XDG_DESKTOP_DIR=/home/${uHome}/Desktop
export XDG_DOWNLOAD_DIR=/home/${uHome}/Downloads
export XDG_TEMPLATES_DIR=/home/${uHome}/Templates
export XDG_PUBLICSHARE_DIR=/home/${uHome}/Public
export XDG_DOCUMENTS_DIR=/home/${uHome}/Documents
export XDG_MUSIC_DIR=/home/${uHome}/Music
export XDG_PICTURES_DIR=/home/${uHome}/Pictures
export XDG_VIDEOS_DIR=/home/${uHome}/Videos
export XDG_CACHE_HOME=/home/${uHome}/.cache
export XDG_CONFIG_HOME=/home/${uHome}/.config
export XDG_DATA_HOME=/home/${uHome}/.local/share
export XDG_STATE_HOME=/home/${uHome}/.local/state
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
export RUSTUP_HOME=/home/${uHome}/.rustup
export CARGO_HOME=/home/${uHome}/.cargo
export BIN_DIR=${BIN_DIR}:-${CARGO_DEFAULT_BIN}
export CARGO_DEFAULT_BIN=${CARGO_HOME}/bin
export PIPX_HOME=${XDG_DATA_HOME}/pipx/venvs
export PIPX_BIN_DIR=${XDG_DATA_HOME}/pipx/bin
export PIPX_MAN_DIR=${XDG_DATA_HOME}/man
export GOROOT=/home/${uHome}/.go
export GOPATH=/home/${uHome}/go
export PATH=${PATH}:${GOROOT}/bin:${GOPATH}/bin
export GO111MODULE=on
export NVM_DIR="/home/${uHome}/.config/nvm"
[ -d /home/${uHome}/.local/bin ] && export PATH=${PATH}:/home/${uHome}/.local/bin
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
