#!/usr/bin/env bash

# ---------------------------------------------- Sources ----------------------------------------------- #

. ~+/lib/colors/colors

# ---------------------------------------------- Variables ----------------------------------------------- #

uHome=$(basename /home/*)

# ---------------------------------------------- Funciones ----------------------------------------------- #
clear

echo ""
echo -e "\t\n${BYELL}[i]${NC}${BCYAN} Inicia la instalacion de Go y el setup de Rust\nAl finalizar deberas reiniciar o salir y reingresar a la sesion${NC}\n"
echo ""
sleep 3

rst_installer() {
  rustup install stable &&
    rustup default stable
}

repos() {
  cd ${HOME}
  git clone https://github.com/sharkdp/pastel.git /home/${uHome}/Downloads/git-repos/pastel
  git clone https://github.com/tarka/xcp.git /home/${uHome}/Downloads/git-repos/xcp
  git clone https://github.com/topgrade-rs/topgrade.git /home/${uHome}/Downloads/git-repos/topgrade
  git clone https://github.com/cvmiller/v6disc.git /home/${uHome}/Downloads/git-repos/v6disc

  cd ~/Downloads/git-repos/topgrade &&
    cargo update &&
    cargo doc --no-deps &&
    cargo install --all-features --path=. &&
    cd ~/
  cd ~/Downloads/git-repos/pastel &&
    cargo update &&
    cargo doc --no-deps &&
    cargo install --all-features --path=. &&
    cd ~/
  cd ~/Downloads/git-repos/xcp &&
    cargo update &&
    cargo doc --no-deps &&
    cargo install --all-features --path=. &&
    cd ~/
  cd ~/Downloads/git-repos/v6disc &&
    ln -s ~+/v4disc.sh /home/${uHome}/.local/bin/v4disc &&
    ln -s ~+/v6disc.sh /home/${uHome}/.local/bin/v6disc &&
    cd ~/
}

rst_installer &&
  repos
