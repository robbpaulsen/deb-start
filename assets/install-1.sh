#!/usr/bin/env bash

##########################################################################################################################
#                                                                                                                        #
# Script for a more unattended, headless setup and configurationg for the basic things on any new kalilinux installation.#
#                                                                                                                        #
##########################################################################################################################

# ---------------------------------------------- Libraries ----------------------------------------------- #

. ~+/lib/colors/colors.sh

# ---------------------------------------------- Variables ----------------------------------------------- #

uHome=$(basename /home/*)

# ---------------------------------------------- Funciones ----------------------------------------------- #
clear

reWrteSrcsLst() {
  sudo sed -i 's/\# deb-src/deb-src/g' /etc/apt/sources.list &&
    echo -n "deb [signed-by=/etc/apt/keyrings/apt-fast.gpg] http://ppa.launchpad.net/apt-fast/stable/ubuntu focal main" | sudo tee --append /etc/apt/sources.list.d/apt-fast.list &&
    sudo mkdir -p /etc/apt/keyrings &&
    sudo curl -fsSL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0xBC5934FD3DEBD4DAEA544F791E2824A7F22B44BD" | gpg --dearmor -o /etc/apt/keyrings/apt-fast.gpg &&
    sudo mkdir -p /etc/apt/keyrings &&
    curl -fsSL https://repo.charm.sh/apt/gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/charm.gpg &&
    echo "deb [signed-by=/etc/apt/keyrings/charm.gpg] https://repo.charm.sh/apt/ * *" | sudo tee /etc/apt/sources.list.d/charm.list
}

usrGrps() {
  echo ""
  echo -e "\t\n${BGREEN}[i]${NC}${BYELL} Agregando al usuario a los grupos relevantes${NC}\n"
  echo ""
  sudo usermod -aG adm,audio,bluetooth,cdrom,dialout,dip,disk,i2c,kaboxer,kismet,kvm,lp,man,messagebus,mosquitto,mysql,pipewire,plocate,postgres,proxy,pulse,pulse-access,redsocks,tcpdump,video,voice,wireshark $(whoami) &>/dev/null
  sleep 2
}

devLayout() {
  echo ""
  echo -e "\t\n${BGREEN}[i]${NC}${BYELL} Generando los directorios usuales de trabajo${NC}\n"
  echo ""
  mkdir -p /home/${uHome}/Projects
  mkdir -p /home/${uHome}/Downloads/git-repos
  mkdir -p /home/${uHome}/.local
  mkdir -p /home/${uHome}/.local/share
  mkdir -p /home/${uHome}/.local/state
  mkdir -p /home/${uHome}/.local/bin
  sleep 2
}


confsPlcmnt() {
  clear
  rm -rf /home/${uHome}/.bashrc &>/dev/null &&
    rm -rf /home/${uHome}/.bash_profile &>/dev/null &&
    rm -rf /home/${uHome}/.profile &>/dev/null &&
    rm -rf /home/${uHome}/.bash_aliases &>/dev/null &&
    rm -rf /home/${uHome}/.zshrc &>/dev/null &&
    rm -rf /home/${uHome}/.zprofile &>/dev/null &&
    rm -rf /home/${uHome}/.config/kitty &>/dev/null &&
    rm -rf /home/${uHome}/.config/nvim &>/dev/null &&
    rsync -rzvhP ~+/conf`s/config/* /home/${uHome}/.config/ &&
    rsync -zvhP ~+/confs/home/zshrc /home/${uHome}/.zshrc &&
    rsync -zvhP ~+/confs/home/bash_aliases /home/${uHome}/.bash_aliases &&
    rsync -zvhP ~+/confs/home/bashrc /home/${uHome}/.bashrc &&
    rsync -zvhP ~+/confs/home/bash_profile /home/${uHome}/.bash_profile &&
    rsync -zvhP ~+/confs/home/profile /home/${uHome}/.profile &&
    rsync -zvhP ~+/confs/home/functions.sh /home/${uHome}/.functions.sh &&
    rsync -zvhP ~+/confs/home/zprofile /home/${uHome}/.zprofile &&
    rsync -zvhP ~+/confs/home/functions.zsh /home/${uHome}/.functions.zsh &&
    rsync -zvhP ~+/confs/home/key-bindings.zsh /home/${uHome}/.key-bindings.zsh &&
    rsync -zvhP ~+/confs/home/tmux.conf /home/${uHome}/.tmux.conf &&
    rsync -rzvhP ~+/confs/home/local/bin/* /home/${uHome}/.local/bin/ &&
    rsync -rzvhP ~+/confs/home/local/share/* /home/${uHome}/.local/share/ &&
  sudo fc-cache -v -f
}

debInstaller() {
  clear
  echo ""
  sudo apt-get --assume-yes update &&
    sudo apt-get --assume-yes full-upgrade &&
    sleep 3
  while read -r line; do
    echo -e "${BYELL}[+]${NC}${BGREEN} Instalando $line${NC}"
    sudo apt-get --assume-yes install "$line" 2>/dev/null
  done <~+/assets/dubuntu.lst
  sudo updatedb
  echo ""
  echo -e "\t\n${BYELL}[i]${NC}${BCYAN} Finalizan las instalaciones${NC}\n"
  echo ""
  sleep 2
}

confsPlcmnt &&
  reWrteSrcsLst &&
  devLayout &&
  usrGrps &&
  debInstaller &&
  sleep 8 &&
  systemctl reboot
